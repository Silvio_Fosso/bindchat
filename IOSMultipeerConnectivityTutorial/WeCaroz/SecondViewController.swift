//
//  SecondViewController.swift
//  IOSMultipeerConnectivityTutorial
//
//  Created by Pierluigi Rizzu on 21/11/2019.
//  Copyright © 2019 Arthur Knopper. All rights reserved.
//

import UIKit

public extension UIView {

    //second view controller diventa il primo
  /**
  Fade in a view with a duration

  - parameter duration: custom animation duration
  */
    func fadeIn(duration duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
        self.alpha = 1.0
    })
  }

  /**
  Fade out a view with a duration

  - parameter duration: custom animation duration
  */
    func fadeOut(duration duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
        self.alpha = 0.0
    })
  }

}


class SecondViewController: UIViewController {
    var username1 = ""
 
    @IBOutlet weak var approfondimento: UIView!
    @IBOutlet weak var usern: UILabel!
    @IBOutlet weak var app1: UILabel!
     var categoriaInfo = ""
     var categoriascelta = ""
    let alert = UIAlertController ( title: "Wait!", message: "You sure you want to pick this category?", preferredStyle:  UIAlertController.Style.alert)

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "passalogin"
    {

        
           let HomePageViewController = segue.destination as? HomePageViewController
        print(uname)
        HomePageViewController?.categoriascelta = categoriascelta
          
           
          
       }
    }
    
    override func viewDidLoad() {
        app1.isHidden = true
            super.viewDidLoad()
        alert.addAction(UIAlertAction(title: "Yes!", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.performSegue(withIdentifier: "passalogin", sender: self)
            
        }))
        alert.addAction(UIAlertAction(title: "No!", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
            print("azione bloccata")
        }))
        
          app1.adjustsFontSizeToFitWidth = true
          app1.minimumScaleFactor = 0.5
          app1.numberOfLines = 0
            // Do any additional setup after loading the view.
        
        }
    
    @IBAction func salvaCategoria(_ sender: UIButton) {
        app1.isHidden = false
          if sender.tag == 1
          {
              categoriascelta = "health"
              app1.text = " Physical and personal problems, suggestions for a better life style."
              approfondimento.fadeIn()
              
                  self.present(self.alert,animated: true)

              
              
              print(categoriascelta)
              
          
          }
         else if sender.tag == 2
          {
                categoriascelta = "cinema&serie"
              app1.text = " Story of cinema, new and old movies and tv series."
              approfondimento.fadeIn()
             
                   self.present(self.alert,animated: true)
                  
              
              
              
              print(categoriascelta)
          }
          
         else if sender.tag == 3
                 {
                  categoriascelta = "culture"
                  app1.text = " Here you can talk about literature, book, politics,ecc.."

                  approfondimento.fadeIn()
               
                      self.present(self.alert,animated: true)
                                     
                    
                  
                  
                  
                  print(categoriascelta)
          }
              
              
          
        else  if sender.tag == 4
                 {
                    categoriascelta = "music"
                  app1.text = " GO LOUD! Talk about any genre of music ( pop, indie, rock, ecc..) and have fun!"
                  approfondimento.fadeIn()
                  
                     self.present(self.alert,animated: true)
                                    
                  
                  
                  
                  print(categoriascelta)
              }
          
         else if sender.tag == 5
                 {
                       categoriascelta = "food"
                  app1.text = " talk about your favourite dishes, and different types of cooking."

                  approfondimento.fadeIn()
                 
                      self.present(self.alert,animated: true)
                                     
                  
                  
                  
                  print(categoriascelta)
                 }
          
              
              
         else if sender.tag == 6
                 {
                      categoriascelta = "sports"
                  app1.text = "Talk about every type of sports ( gym, basket, soccer, ecc..) and news."

                  approfondimento.fadeIn()
                  
                     
                      self.present(self.alert,animated: true)
                                     
                  
                  
                  
                  print(categoriascelta)
                 }
              
         else
                 {
                    categoriascelta = "game"
                  app1.text = " Talk about games, story of video games, esports and news."

                  approfondimento.fadeIn()
                   self.present(self.alert,animated: true)
                                     
                      
                  
                  
                  
                  print(categoriascelta)
                  }
          
              }
    

    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
