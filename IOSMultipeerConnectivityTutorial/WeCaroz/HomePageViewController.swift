//
//  HomePageViewController.swift
//  IOSMultipeerConnectivityTutorial
//
//  Created by Pierluigi Rizzu on 12/11/2019.
//  Copyright © 2019 Arthur Knopper. All rights reserved.
//

import UIKit
extension UIViewController {
    /// Call this once to dismiss open keyboards by tapping anywhere in the view controller
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }

    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        
        return tap
    }
}



class HomePageViewController: UIViewController,UITextFieldDelegate {
    
    var user1 = ""
    var categoriascelta = ""
    var username1 = ""

    @IBOutlet weak var campoUser: UITextField!

    @IBOutlet weak var argomento: UILabel!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                                  if segue.identifier == "trovadispositivi"
                                  {
                                      
                                      var uname = ""
                                      username1 = username1.uppercased()
                                    
                                    print("USERNAME IN MAIUSCOLO")
                                   print(username1)
                                    
                                   categoriascelta = categoriascelta.uppercased()
                                    print(categoriascelta)
                                      uname = username1 + "_" + categoriascelta
                                      print("guarda")
                                    print(uname)
                                         let ViewController = segue.destination as? ViewController
                                      print(uname)
                                      ViewController?.username = uname
        }
    }
    
    @IBAction func passaacategoria(_ sender: Any) {
        
        
        if(campoUser.text?.isEmpty == false)
               {
                
                
                campoUser.text?.replacingOccurrences(of: "_", with: " ",options: NSString.CompareOptions.literal,range: nil)
                print("username senza _")
                print(campoUser.text)
                username1 = campoUser.text!
              
                
               performSegue(withIdentifier: "trovadispositivi", sender: self)
                   print(campoUser.text)
                   print(username1)
               }
               else
               {
                   print("La label è vuota.")
               }
    }
   
                                     
                            
    override func viewDidLoad() {
        argomento.text = categoriascelta
        print(categoriascelta)
        super.viewDidLoad()
        self.campoUser?.delegate = self
            self.setupHideKeyboardOnTap()
            
            
            
            campoUser?.addTarget(nil, action: "firstResponderAction:", for: .editingDidEndOnExit)
            campoUser?.addTarget(nil, action: #selector(onReturn), for: .editingDidEndOnExit)

                NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            }
        
        @objc func onReturn () {
                 self.campoUser.resignFirstResponder()
                 print("gatto")
             }

            @objc func keyboardWillShow(notification: NSNotification) {
                if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                    if self.view.frame.origin.y == 0 {
                        self.view.frame.origin.y -= keyboardSize.height
                    }
                }
            }

            @objc func keyboardWillHide(notification: NSNotification) {
                if self.view.frame.origin.y != 0 {
                    self.view.frame.origin.y = 0
                    
            }
        }

}
