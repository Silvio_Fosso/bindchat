//
//  ViewController.swift
//  IOSMultipeerConnectivityTutorial
//
//  Created by Arthur Knopper on 17/05/2019.
//  Copyright © 2019 Arthur Knopper. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import CoreBluetooth
class ViewController: UIViewController, MCSessionDelegate, MCBrowserViewControllerDelegate , UITableViewDataSource , UITableViewDelegate, MCNearbyServiceBrowserDelegate,MCNearbyServiceAdvertiserDelegate,UITextFieldDelegate, UIApplicationDelegate
{
    @IBOutlet weak var compagnia: UILabel!
    //   TE LO COMMENTO VISTO CHE NON MI VA DI METTERTELO A FUNZIONI
    
    var cont = 0
    var s = [String]()
    var string = ""
    var boole = false
    var arrint = [Int]()
    var nomicopia = [String]()
    @IBOutlet weak var search: UITextField!
    @IBOutlet weak var welcomeBack: UILabel!
    var mcp = [MCPeerID]()
    var mcpcopia = [MCPeerID]()
    var nones = ""
    var bos = false
    var timer = Timer()
    var sess : MCSession!
    var activesess = false
    var name = ""
    var nom = false
    var username = ""
    @IBOutlet weak var contatore: UILabel!
    
   var usernam = ""
    @IBOutlet weak var tb: UITableView!
    
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
//        QUI è QUANDO TROVA UN PEER
        tb.reloadData()
//        lo metto in nomi perchè è quello che popola la table view
        nomi.append(peerID.displayName)
//        mi serve per invitare gente nel club
        mcp.append(peerID)
       print(peerID)
        tb.beginUpdates()
        tb.insertRows(at: [IndexPath(row: nomi.count-1, section: 0)], with: .automatic)
        tb.endUpdates()
        for a in 0...nomi.count-1
        {
            let arr = Array(nomi[a])
            for b in 0...arr.count-1
            {
            if(boole == true)
            {
                string+=String(arr[b])
            }
            if(arr[b] == "_")
            {
             boole = true
            }
            
            }
            boole = false
            s.append(string)
            string=""
//            questa funzione prende gli interessi  dei peer trovati e li mette in un array
        }
                    search.text = usernam.uppercased()

                     if (search.text != "")
                     {
                     for a in 0...s.count-1
                     {
                         if(search.text == s[a])
                         {
                             arrint.append(a)
                             
                         }
                     }
//                         qui confronta il tuo interesse con quello dei peer e si salva la posizione dell'array
                     s.removeAll()
                    
                      nomicopia = nomi
                      mcpcopia = mcp
                        mcp.removeAll()
                     nomi.removeAll()
//                        ovviamente svuoto gli array perchè mi servono per ripopolare la table view con le persone con i miei stessi interessi
                     for a in arrint
                     {
                        print(arrint)
                         
                            self.nomi.append(self.nomicopia[a])
                        
                            self.mcp.append(self.mcpcopia[a])
                            browser.invitePeer(self.mcp[a], to: self.mcSession, withContext: nil, timeout: 10.0)
                            
//                         li metto nella table view mettendoli in nomi essendo che la table view si basa su il contenuto di nomi e poi li autoinvito nella sessione
                     }
                        arrint.removeAll()
                    tb.reloadData()
                    
                        
                     
        }
                    
        
        
        
        
      
       
         
    }
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        
    }
    
   
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print(peerID)
        var i = 0
        if(nomi.count>0)
        {
        for a in 0...nomi.count-1
        {
            if(nomi[a] == peerID.displayName)
            {
               i = a
               
            }
        }
            
       
        nomi.remove(at: i)
        mcp.remove(at: i)
        tb.beginUpdates()
        tb.deleteRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
        tb.endUpdates()
//            semplicemente quando perdo la connessione li tolgo dalla table view
        }
        
    
  
    }
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print(error.localizedDescription)
    }
    
   func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        
       invitationHandler(true, mcSession)
//         ricezione invito
    
    
   
    }
    
    @IBAction func ricerca(_ sender: Any) {
        
        
    }
    
    @IBAction func editing(_ sender: Any) {
       
        
        print(s)
        
    }
    
    var foundPeers = [MCPeerID]()
    var browser: MCNearbyServiceBrowser!

    var advertiser: MCNearbyServiceAdvertiser!
    var nomi = [String]()
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        peerID1 = mcp[indexPath.row]
        
//        browser.invitePeer(peerID1, to: mcSession, withContext: nil, timeout: 10.0)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        print(nomi.count)
        contatore.text = String(nomi.count)
        return nomi.count
        
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        cell?.textLabel?.text = nomi[indexPath.row]
        return cell!
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           let displayVC = segue.destination as! ViewController3
           displayVC.s = nones
            displayVC.mcSession = sess
            displayVC.adv = advertiser
           displayVC.browser = browser
//    mi passo nell'altra view tutto il necessario per invitare altra gente
   }
  
  @IBAction func tapSendButton(_ sender: Any) {
    
    let message = messageToSend.data(using: String.Encoding.utf8, allowLossyConversion: false)
    
    do {
      try self.mcSession.send(message!, toPeers: self.mcSession.connectedPeers, with: .unreliable)
    
    }
    catch {
      print("Error sending message")
    }
  }
  
  var peerID1: MCPeerID!
  var mcSession: MCSession!
  var mcAdvertiserAssistant: MCAdvertiserAssistant!
  var messageToSend: String!
  
  
  
 var manager:CBCentralManager!
  func applicationWillTerminate(_ application: UIApplication) {
      mcSession.disconnect()
    advertiser.stopAdvertisingPeer()
    browser.stopBrowsingForPeers()
  }

  override func viewDidLoad() {
    search.isHidden = true
    super.viewDidLoad()
    print(username)
    self.navigationItem.hidesBackButton = true
    nomi.removeAll()
   
    let notificationCenter = NotificationCenter.default
//    questi 2 mi servono per quando l'app va in background e torna aperto
     notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
    notificationCenter.addObserver(self, selector: #selector(AppReturnedToForegorund), name: UIApplication.didBecomeActiveNotification, object: nil)
    search.delegate = self
    tb.delegate = self
    var c = Array(username)
    for a in 0...c.count-1
    {
        if(nom)
        {
            usernam+=String(c[a])
        }
        if(c[a] == "_")
        {
            nom = true
        }
    }
//    recupero interesse personale
    

    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(showConnectionMenu))
    
    peerID1 = MCPeerID(displayName: username)
    mcSession = MCSession(peer: peerID1, securityIdentity: nil, encryptionPreference: .required)

//    let mcBrowser = MCBrowserViewController(serviceType: "ioscreator-chat", session: mcSession)
    mcSession.delegate = self
    browser = MCNearbyServiceBrowser(peer: peerID1, serviceType: "ioscreator-chat")
    browser.delegate = self
    
        advertiser = MCNearbyServiceAdvertiser(peer: peerID1, discoveryInfo: nil, serviceType: "ioscreator-chat")
       
    advertiser.delegate = self
    
    
    browser.startBrowsingForPeers()
//    questo cerca i peer
    advertiser.startAdvertisingPeer()
//    questo cerca inviti e li accetta
    
  
  }
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    @objc func AppReturnedToForegorund()
    {
        browser.startBrowsingForPeers()
        advertiser.startAdvertisingPeer()
    }
    @objc func appMovedToBackground() {
        browser.stopBrowsingForPeers()
        advertiser.stopAdvertisingPeer()
    }

   
    // override func viewDidAppear(_ animated: Bool) {
     // if(Wifi())
     // {
      //    self.navigationItem.hidesBackButton = true
      // }else{
       //   self.navigationItem.hidesBackButton = false
     // }
    // }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        compagnia.text = "If noone is here, consider to change your category."
        compagnia.minimumScaleFactor = 0.5
        compagnia.adjustsFontSizeToFitWidth = true
        compagnia.numberOfLines = 0
    }
        
    
    func Wifi() -> Bool {
        var address : String?
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                     let name = String.init(cString: (interface?.ifa_name)!)
                     if name == "awdl0"{
                        if((Int32(interface!.ifa_flags) & IFF_UP) == IFF_UP) {
                            return(true)
                        }
                        else {
                            return(false)
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return (false)
    }
  
  @objc func showConnectionMenu() {
    let ac = UIAlertController(title: "Connection Menu", message: nil, preferredStyle: .actionSheet)
    ac.addAction(UIAlertAction(title: "Join an exsisting session", style: .default, handler: joinSession))
    ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
    present(ac, animated: true)
  }
  
 
  
  func joinSession(action: UIAlertAction) {
    
 
    
  }
   
   
  
  func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
    switch state {
    case .connected:
      print("Connected: \(peerID.displayName)")
      DispatchQueue.main.sync {
         nones = peerID1.displayName
        sess = session
        browser.stopBrowsingForPeers()
//        advertiser.stopAdvertisingPeer()
         performSegue(withIdentifier: "parliamo", sender: self)
       
      }
    case .connecting:
      print("Connecting: \(peerID.displayName)")
    case .notConnected:
      print("Not Connected: \(peerID.displayName)")
    @unknown default:
      print("fatal error")
    }
  }
  
  func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
    DispatchQueue.main.async { [unowned self] in
      // send chat message
      let message = NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue)! as String
     
    }
  }
 
  func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    
  }
  
  func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    
  }
  
  func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    
  }
    
    override func viewDidDisappear(_ animated: Bool) {
       
    }
    
  func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
    dismiss(animated: true)
  }
  
  func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
    dismiss(animated: true)
  }
    
    func browserViewController(_ browserViewController: MCBrowserViewController, shouldPresentNearbyPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) -> Bool {
        
        nomi.append(peerID.displayName)
       
        
        return true
    }
    
   

}

